#!/bin/sh

set -e

# Checks
twine -h > /dev/null
VERSION=$(./blaeu-reach --version | cut -d' ' -f3)
if [ -z "$VERSION" ]; then
   echo "No version found" >&2
   exit 1
fi
if $(echo $VERSION | grep BETA > /dev/null); then
   echo "Still a beta version ($VERSION)"  >&2
   exit 1
fi
VERSION2=$(python3 setup.py --version)
if [ "$VERSION" != "$VERSION2" ]; then
    echo "$VERSION and $VERSION2 are different"  >&2
   exit 1
fi
PACKAGE=blaeu
# git commit returns 1 if there is nothing to commit. We use
# diff-index to check that everything is committed
git diff-index --quiet HEAD

# Now, let's actually do things
git tag release-${VERSION}
python3 setup.py sdist
gpg --detach-sign -a dist/${PACKAGE}-${VERSION}.tar.gz
twine upload dist/${PACKAGE}-${VERSION}.tar.gz dist/${PACKAGE}-${VERSION}.tar.gz.asc
git push
# Tags are not pushed by 'git push' :-(
git push origin release-${VERSION}
