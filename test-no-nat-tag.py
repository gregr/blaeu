#!/usr/bin/env python3

# A simple example on how to use the Blaeu package from your own
# programs.

# Here, we request only probes without the "nat" user tag and we look
# at their source address to see if it is really a public one (not RFC
# 1918). Warning, many user tags are wrong (most probes are in a
# NATted network, but the host forgot to add the tag "nat").

NUM=200

import Blaeu

from netaddr import *

import sys
import time

def usage(msg=None):
    print("Usage: %s target-name-or-IPv4" % sys.argv[0], file=sys.stderr)
    config.usage(msg)

# RFC 1918
prefix1 = IPNetwork('10.0.0.0/8')
prefix2 = IPNetwork('172.16.0.0/12')
prefix3 = IPNetwork('192.168.0.0/16')
    
def is_public(str):
    return IPAddress(str) not in prefix1 and IPAddress(str) not in prefix2 and \
      IPAddress(str) not in prefix3
    
config = Blaeu.Config()
(args, data) = config.parse("", []) # We don't use specific options
if len(args) != 1:
    usage("Not the good number of arguments")
    sys.exit(1)
target = args[0]
data["definitions"][0]["type"] = "ping"
data["definitions"][0]["description"] = "Test of no user tag \"nat\" on %s" % target
data["definitions"][0]['af'] = 4
del data["definitions"][0]["port"]
data["definitions"][0]["packets"] = 1
data["definitions"][0]["target"] = target
if config.include is not None:
    data["probes"][0]["tags"]["include"] = copy.copy(config.include)
    data["probes"][0]["tags"]["include"].append("system-ipv4-works")
else:
    data["probes"][0]["tags"]["include"] = ["system-ipv4-works"]
if config.exclude is not None:
    data["probes"][0]["tags"]["exclude"] = copy.copy(config.exclude)
    data["probes"][0]["tags"]["exclude"].append("nat")
else:
    data["probes"][0]["tags"]["exclude"] = ["nat"]
data["probes"][0]["requested"] = NUM
measurement = Blaeu.Measurement(data)
rdata = measurement.results(wait=True, percentage_required=config.percentage_required)
print(("%s probes reported" % len(rdata)))
for result in rdata:
    addr = result['src_addr']
    if not is_public(addr):
        print("Probe %s has address %s which is not public" % (result['prb_id'], addr))
print(("Test #%s done at %s" % (measurement.id,
                                    time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()))))


